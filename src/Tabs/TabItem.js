import React from 'react';
import "./Tabs.scss"
const TabItem = props => {
    return (
        <div className={'tabs-content-item'}>
            <p className="tabs-content-text">{props.text}</p>
        </div>

    );
};

export default TabItem;