import React, {Component} from 'react';
import TabsContainer from "./Tabs/TabsContainer";
import "./Tabs/Tabs.scss"
class App extends Component {
    tabsArray =
        [
            {
                title: 'Tab 1',
                content: 'content 1. Lorem ipsum dolor sit amet....'
            },
            {
                title: 'Tab 2',
                content: 'content 2. Lorem ipsum dolor sit amet....'
            },
            {
                title: 'Tab 3',
                content: 'content 3. Lorem ipsum dolor sit amet....'
            },
            {
                title: 'Tab 4',
                content: 'content 4. Lorem ipsum dolor sit amet....'
            }

        ];

    render() {
        return (
            <div>
                <TabsContainer tabs={this.tabsArray}/>
            </div>
        );
    }
}
export default App;
