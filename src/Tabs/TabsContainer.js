import React, {Component} from 'react';
import TabHeader from "./TabHeader";
import TabItem from "./TabItem";
import "./Tabs.scss"
class TabsContainer extends Component {

    state={
        activeTabs : 0
    };

        render() {
            let headers=[];
            let activeContent=<TabItem
            text={this.props.tabs[this.state.activeTabs].content}
            />;
            this.props.tabs.forEach((t,ind)=>{
                headers.push(<TabHeader
                    key={ind}
                    text={t.title}
                    click={()=>this.setState({activeTabs:ind})}
                />)
                }

            );

            return (
                <div className={"tabs-container"}>
                    <div className={"tabs-header"}>{headers}</div>
                    <div className={"tabs-content"}>{activeContent}</div>
                </div>
            );
        }
}

export default TabsContainer;