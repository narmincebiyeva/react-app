import Message from "./Message";
import AuthInput from "./AuthInput";
import React, {useState} from "react";

const EmailService = props => {

    const [state, setState] = useState({
        isCorrectEmail: false,
        isCorrectPassword: false
    });
    const validateEmail = (e) => {
        const allowedEmailServices = props.allowedEmailServices;
        const usersEmailServiceAddress = e.target.value.split('@')[1];
        setState(prevState => ({
            ...prevState,
            isCorrectEmail: allowedEmailServices.some((e) => e === usersEmailServiceAddress)
        }))
    };

    const validatePassword = (e) => {
        const answer = e.target.value.length >= 8;

        setState(prevState => {
            return {
                ...prevState,
                isCorrectPassword: answer
            }
        })
    };

    return (
            <form onSubmit={props.submitHandler}>
                <AuthInput type={'email'} validation={validateEmail}/>
                <AuthInput type={'password'} validation={validatePassword}/>

                <input disabled={!(state.isCorrectEmail && state.isCorrectPassword)}
                       type="submit"
                       value={'Log in'}/>
            </form>

            );
};
