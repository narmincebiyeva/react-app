import React, {Component} from 'react';
import {Link,Route} from "react-router-dom";

class App extends Component {

   user=()=>{
       return(
               <h1>User</h1>

           )
   };
   login=()=>{
       return(
           <h1>login</h1>
       )
   };
   register=()=>{
       return(
           <h1>register</h1>
       )
   };

    render() {
        return (
            <div>
                <>
                    <h2>React route is a separate library!</h2>
                    <p>We need to download it every time for every project</p>
                    <p>download command - npm install react-router react-router-dom --save</p>
                    <Link to={'/user'}>Home</Link>
                    <Link to={'/login'}>Login</Link>
                    <Link to={'/register'}>Register</Link>
                    <Route path={"/user"} component={this.user}/>
                    <Route path={"/login"} component={this.login}/>
                    <Route path={"/register"} component={this.register}/>
                </>

            </div>

        );
    }
}

export default App;
