import React, {useState} from 'react';
import BurgerButton from "./BurgerButton";
import BurgerMenu from "./BurgerMenu";
import "./burgerMenu.scss"
import PropTypes from "prop-types"

const BurgerSystem = props =>{
    const [BurgerMenuState,setBurgerMenuState] = useState({
        show :false
    });

    const MenuHandler = () =>{
      setBurgerMenuState({open: !BurgerMenuState.show});
    };

        return (
          <>
              <BurgerButton clickHandler= {MenuHandler}/>
              {
                  BurgerMenuState.show?<BurgerMenu items = {props.menuItems}/>
                  :null
              }

          </>
        );

};
BurgerSystem.defaultProps ={
  menuItems:["items1","items2","items3"]
};

BurgerSystem.propTypes = {
    menuItems: PropTypes.arrayOf(PropTypes.string)
};

export default BurgerSystem;





// class BurgerSystem extends Component {
//     state = {
//         show :false
//     };
//     MenuHandler = () =>{
//         this.setState({
//             show: !this.state.show
//         })
//     };
//     render() {
//         return (
//             <>
//                 <BurgerButton clickHandler={this.MenuHandler}/>
//                 {
//                     this.state.show?<BurgerMenu items = {this.props.menuItems}/>
//                         :null
//
//                 }
//
//             </>
//         );
//     }
// }
// BurgerSystem.defaultProps ={
//     menuItems:["items1","items2","items3"]
// };
//
// BurgerSystem.propTypes = {
//     menuItems: PropTypes.arrayOf(PropTypes.string)
// };
//
// export default BurgerSystem;