import React from 'react';

const BurgerMenu = props => {
    const  map = props.items.map((s,index) => <div key={index} className={"menu-item"}>{s}</div>);
    return (
        <div className={"menu"}>
            {map}
        </div>
    );
};

export default BurgerMenu;