import React from 'react';

const BurgerButton = props => {
    return (
        <div>
            <button className={"burger-button"} onClick={props.clickHandler}>
                <div className={"line"}></div>
                <div className={"line"}></div>
                <div className={"line"}></div>
            </button>

        </div>
    );
};

export default BurgerButton;