import React from 'react';

const VoteItem = props=> {
    return (
        <div onClick={props.handleVote}>
            <span>{props.name}</span>
            <span>{props.votesCount}</span>
        </div>
    );
};

export default VoteItem;