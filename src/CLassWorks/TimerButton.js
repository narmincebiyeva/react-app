import React from 'react';

const TimerButton = props => {
    return (
        <button onClick={props.clickHandler}>
            {props.name}
        </button>
    );
};

export default TimerButton;