import React,{Component} from "react";
import VoteItem from "./VoteItem";

class  VoteSystem extends Component{
    constructor(props) {
     super(props);
     const  voteItems = props.items.map(voteItem => ({name: voteItem,votesCount: 0}));
    this.state = {
        votes: [...voteItems]
    };
    this.title = React.createRef();
    this.message = React.createRef();


    }

    handleVoteCLick =(e)=>{
        const votes = [...this.state.votes];
        const  voteItemName =e.currentTarget.firstChild.textContent;
         const votedItemFromState = votes.find( vI => vI.name === voteItemName);
         votedItemFromState.votesCount++;

         this.title.current.style.color="red";

         setTimeout(()=>{
             this.title.current.style.color=null;
         },2000);

         this.message.current.innerText = "you voted for " + votedItemFromState.name;
         setTimeout(()=>{
             this.message.current.innerText = null;
         },5000);
         this.setState(
             {
                 votes: votes
             }
         );

    };
    render() {
        let  voteItems = this.state.votes;
        voteItems = voteItems.map((voteObj,ind )=>
            <VoteItem
                key = {ind}
                name = {voteObj.name}
                votesCount = {voteObj.votesCount}
                handleVote = {this.handleVoteCLick}
            />

                );

        return(
            <div className={"votes-container"}>
                {voteItems}
                <h2 className={"votes-title"} ref={this.title}>Vote for something</h2>
                <p ref={this.message}> </p>
            </div>
        );
    }
}
export default VoteSystem