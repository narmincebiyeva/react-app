import React, {Component} from 'react';
import TimerButton from "./TimerButton";

class Timer extends Component {
    state = {
        time: 0,
        intervalId :null
    };
    time = React.createRef();

    startHandler = (e) =>
    {
        let currentInterval = this.state.intervalId;
        currentInterval = setInterval(() =>
            {
                let currentTime = this.state.time;
                this.setState({time: ++currentTime});
            },1000);
        this.setState({intervalId:currentInterval});
    };
    stopHandler = (e) =>
    {
        clearInterval(this.state.intervalId);
    };
    resetHandler = (e) =>
    {
      clearInterval(this.state.intervalId);
      this.setState({time: 0 });
    };

    render() {
        return (
            <div>
                <p>{this.state.time}</p>
                <TimerButton name="start" clickHandler = {this.startHandler}/>
                <TimerButton name="stop" clickHandler = {this.stopHandler}/>
                <TimerButton name="reset" clickHandler = {this.resetHandler}/>
            </div>
        );
    }
}

export default Timer;