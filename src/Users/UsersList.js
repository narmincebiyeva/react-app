import React, {Component} from 'react';
import User from "./User";
import Preloader from "./Preloader";


class UsersList extends Component {
    state = {
        users: []
    };

    componentDidMount() {
       const request = new XMLHttpRequest();
       request.open('GET', this.props.url);

         request.onreadystatechange = () => {
           if (request.readyState === 4 && request.status === 200) {
             this.setState({
               users: JSON.parse(request.response)
             })
           }
         };

         request.send();

         fetch(this.props.url).then(r => r.json())
           .then(data => {
             this.setState({
               users: data
             })
           });



    }

    render() {
        const users = this.state.users.map((u, ind) => <User key={ind} object={u}/>);

        return (
            <div className={'users-wrapper'}>
                {users.length === 0 ?
                    <Preloader/>
                    : users
                }
            </div>
        );
    }
}

export default UsersList;

