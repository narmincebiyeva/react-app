import React from 'react';

const User = props => {
    return (
        <div className={'user-item'}>
            <p>Name - {props.object.name}</p>
            <p>Surname - {props.object.surname}</p>
            <p>Email - {props.object.email}</p>
        </div>
    );
};

export default User;
