import React, {Component} from 'react';
import './ModalWindow.scss'
class ModalWindow extends Component {
    prevent =(e)=>{
        e.stopPropagation();
    };


    render() {
        return (
            <>
                  <div className="modal" onClick={this.props.closed}>

                        <div className={"modal-window"} onClick={this.prevent}>
                            <form  onSubmit={this.props.submitHandler}>
                                <div  className={"modal-header"}>
                                    <p>{this.props.headertext}</p>
                                    <div onClick={this.props.closed} className="modal-close">X</div>
                                </div>
                                <input type="text" id={'subject'} className="modal-input" placeholder={'subject'}/>
                                <input type="email" id={'mailFrom'} className="modal-input" placeholder={'from'}/>
                                <input type="email" id={'mailTo'} className="modal-input" placeholder={'to'}/>
                                <textarea name="text" defaultValue={'type your text here'} id="text" cols="30" rows="10"/>
                                <input type="submit" value={"Send"} id={"send"}/>
                            </form>

                        </div>
                   </div>

            </>
        );
    }
}


export default ModalWindow;