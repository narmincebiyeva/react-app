import React, {useState} from 'react';
import Email from "./Email";
const MailList = props => {
    const emails = props.emails.map((email, ind) => <Email key={ind} self={email}/>);
    const [toggleFolderCurrentVal, setToggleFolder] = useState({toggleFolder: false});

    const toggleFolderHandler = () => {
        setToggleFolder(!toggleFolderCurrentVal);
    };

    return (
        <div className={'emails'}>
            <h2 className={'emails-folder-name'} onClick={toggleFolderHandler}>{props.folderTitle}</h2>
            <div className={'emails-container'} hidden={toggleFolderCurrentVal}>
                {emails}
            </div>
        </div>
    )
};

export default MailList;
