import './ModalWindow.scss'
import React, {Component} from 'react';

class Buttons extends Component {
    render() {
        return (
            <div className={"modal-button"} style={{backgroundColor: this.props.color}} onClick={this.props.clicked}>
                {this.props.text}
            </div>
        );
    }
}

export default Buttons;
