import React, {Component} from 'react';
import MailList from "./MailList";
import Buttons from "./Buttons";
import ModalWindow from "./ModalWindow";
import Preloader from "../Users/Preloader";
class MainApp extends Component {
    state = {
        emails:{},
        modalWindow:false
    };

    showModal = (e) => {
        this.setState({
            modalWindow:!this.state.modalWindow,
        });

        const  inputs =[...e.target.children],
            newEmail={};
        inputs.forEach((input,ind) => {
                if (ind !== inputs.length - 1) {
                    newEmail[input.id] = input.value

                }
            }
        );
        fetch("http://mail.danit.com.ua/email/draft/new",
            {method:"POST",body:JSON.stringify(newEmail)}).then(s => s.json())
            .then(newEmail =>
            {
                if(Object.keys(newEmail).length>0)
                {
                    const  newEMails ={...this.state.emails};
                    newEMails.draft.push(newEMails);
                    this.setState({emails:newEMails});

                }
            });

    };
    send = (e) => {

        e.preventDefault();
        const  inputs =[...e.target.children],
            newEmail={};
        inputs.forEach((input,ind) => {
                if (ind !== inputs.length - 1) {
                    newEmail[input.id] = input.value

                }
            }
        );
        fetch("http://mail.danit.com.ua/email/send/new",
            {method:"POST",body:JSON.stringify(newEmail)}).then(s => s.json())
            .then(newEmail =>
            {
                if(Object.keys(newEmail).length>0)
                {
                    const  newEMails ={...this.state.emails};
                    newEMails.send.push(newEMails);
                    this.setState({emails:newEMails});

                }
            });
        this.showModal();

    };

    componentDidMount() {
        fetch("http://mail.danit.com.ua/email")
            .then(r => r.json())
            .then((data) => {
                this.setState({emails:{...data}});

            });
    }


    render() {
        const emails = {...this.state.emails};
        const emailLists = Object.keys(emails).map((pN, ind) => {
            return <MailList key={ind} emails={emails[pN]} folderTitle={pN.toUpperCase()}/> });



        return (
            <>
                {
                    Object.keys(emails).length > 0 ?
                        emailLists
                        // <div>
                        //     <MailList folderTitle={"Inbox"} emails={emails.inbox}/>
                        //     <MailList folderTitle={"Sent"} emails={emails.send}/>
                        //     <MailList folderTitle={"Draft"} emails={emails.draft}/>
                        //     <MailList folderTitle={"Trash"} emails={emails.trash}/>
                        //
                        // </div>
                        : <Preloader/>
                }
                <Buttons text={"New Email"} color={"#b374a8"} clicked={this.showModal}/>
                {
                    this.state.modalWindow ?
                        <ModalWindow submitHandler={this.send} closed={this.showModal}
                                     headertext={"Enter emails"}

                        /> : null
                }
            </>
        );
    }
}
export default MainApp;
